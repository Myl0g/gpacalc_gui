//
//  Helpers.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/17/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Foundation

/// NSHomeDirectory resolves to ~/Library/Containers/org.nbps.gpacalc/Data/
let prefsFile = "\(NSHomeDirectory())/Documents/gradebook.json"

func fileExists(_ path: String) -> Bool {
    return FileManager.default.fileExists(atPath: path);
}

func prefsExist() -> Bool {
    return fileExists(prefsFile);
}
func makePrefs() {
    FileManager.default.createFile(atPath: prefsFile, contents: nil, attributes: nil)
}

func sandboxedFile(_ name: String) -> URL {
    
    let fileManager = FileManager.default;

    if !prefsExist() {
        fileManager.createFile(atPath: prefsFile, contents: nil, attributes: nil)
    }
    
    // The following was copy-pasted from StackOverflow. It just works.
    if let dir = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
        return dir.appendingPathComponent(name)
    }
    
    return URL(string: "")!;
}

func readSandboxedFile(name: String) -> Data? {
    do {
        return try Data(contentsOf: sandboxedFile(name))
    }
    catch {
        print(error);
    }

    return nil;
}

func writeSandboxedFile(name: String, data: Data) {
    do {
        try data.write(to: sandboxedFile(name))
    }
    catch {
        print(error);
    }
}
