//
//  Grade.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/17/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Foundation

/**
 Grade contains the grade for a single course. It stores:
 
 * Course name
 * Course grade
 * Course weight (based on rigor)
 
 The main constructor takes all arguments in string form. The abnormal constructor takes all arguments in their eventual Double form.
 
 Here's how a grade works:

 * Letter is either A, B, C, D or F, representing 4.00 - 0.00 respectively.
 * Symbol is + or -, and 0.33/-0.33 respectively.
 * Type is 1.00, 1.15, or 1.30 depending on course rigor (acts as a multiplier).
 
 Grade implements Codable so that it can be easily converted to JSON for storage. Actual serialization happens in the Gradebook class.
*/
final public class Grade: Codable {
    var courseName: String;
    var letterReadable: String;
    var symbolReadable: String;
    var typeReadable: String;
    
    var letter: Double;
    var symbol: Double;
    var type: Double;
    
    init(name: String, letter: String, symbol: String, type: String) {
        self.courseName = name;
        
        /// All of the decimal values used below are from the official North Broward GPA website.
        
        self.letterReadable = letter;
        switch letter {
        case "A":
            self.letter = 4.00;
        case "B":
            self.letter = 3.00;
        case "C":
            self.letter = 2.00;
        case "D":
            self.letter = 1.00;
        default:
            self.letter = 0.00;

        }
        
        self.symbolReadable = symbol;
        switch symbol {
        case "+":
            self.symbol = 0.33;
        case "-":
            self.symbol = -0.33;
        default:
            self.symbol = 0.00;
            self.symbolReadable = ""
        }

        self.typeReadable = type;
        switch type {
        case "CP":
            self.type = 1.00;
        case "Honors":
            self.type = 1.15;
        case "Accelerated":
            self.type = 1.30;
        default:
            self.type = 0.00;
            self.typeReadable = "";
        }
    }
    
    public var serialized: Data {
        let encoder = JSONEncoder()
        do {
            return try encoder.encode(self);
        } catch {
            print(error);
        }
        return Data();
    }
    
    public var description: String {
        return "\(courseName): \(letterReadable)\(symbolReadable) (\(typeReadable))"
    }
}
