//
//  BaseWindowController.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/18/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Cocoa

var gradebook: Gradebook = Gradebook();

final class BaseWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

}
