//
//  EditCoursesViewController.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/18/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Cocoa

final class EditCoursesViewController: NSViewController {
    @IBOutlet weak var coursesPopUp: NSPopUpButton!
    @IBOutlet weak var letterChooser: NSPopUpButton!
    @IBOutlet weak var symbolChooser: NSPopUpButton!
    @IBOutlet weak var typeChooser: NSPopUpButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    override func viewDidAppear() {
        coursesPopUp.removeAllItems();
        letterChooser.removeAllItems();
        symbolChooser.removeAllItems();
        typeChooser.removeAllItems();

        for grade in gradebook.grades {
            coursesPopUp.addItem(withTitle: grade.courseName);
        }
        
        letterChooser.addItem(withTitle: "A")
        letterChooser.addItem(withTitle: "B")
        letterChooser.addItem(withTitle: "C")
        letterChooser.addItem(withTitle: "D")
        letterChooser.addItem(withTitle: "F")
        
        symbolChooser.addItem(withTitle: "+")
        symbolChooser.addItem(withTitle: "-")
        symbolChooser.addItem(withTitle: "None")
        
        typeChooser.addItem(withTitle: "CP")
        typeChooser.addItem(withTitle: "Honors")
        typeChooser.addItem(withTitle: "Accelerated")
        
        changeInSelectedItem(coursesPopUp)
    }
    
    @IBAction func changeInSelectedItem(_ sender: NSPopUpButton) {
        guard let selectedName = coursesPopUp.titleOfSelectedItem, let index = gradebook.findCourseIndex(name: selectedName) else {
            return;
        }

        let grade = gradebook.grades[index]
        
        letterChooser.selectItem(withTitle: grade.letterReadable)
        symbolChooser.selectItem(withTitle: grade.symbolReadable)
        typeChooser.selectItem(withTitle: grade.typeReadable)
    }
    
    @IBAction func submitEdit(_ sender: NSButton) {
        guard let selectedName = coursesPopUp.titleOfSelectedItem, let index = gradebook.findCourseIndex(name: selectedName) else {
            return;
        }

        gradebook.grades[index] = Grade(name: coursesPopUp.titleOfSelectedItem!, letter: letterChooser.titleOfSelectedItem!, symbol: symbolChooser.titleOfSelectedItem!, type: typeChooser.titleOfSelectedItem!)
        gradebook.save();
    }
    
}
