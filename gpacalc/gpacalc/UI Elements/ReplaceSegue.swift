import Cocoa

/**
 The following segue is instantaneous and overwrites the current window with the contents of the view controller it's seguing to.
 In that sense, it behaves in a way very similar to the iOS Show segue.
*/
final class ReplaceSegue: NSStoryboardSegue {
    override func perform() {
        if let src = self.sourceController as? NSViewController,
            let dest = self.destinationController as? NSViewController,
            let window = src.view.window {
            // this updates the content and adjusts window size
            window.contentViewController = dest
        }
    }
}
