//
//  Update.swift
//  gpacalc
//
//  Created by Milo Gilad on 10/18/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import Foundation

func isUpToDate() -> Bool {
    let url = URL(string: "https://myl0g.keybase.pub/gpacalc_gui/VERSION");
    let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    
    do {
        let latestVersionRaw = try String(contentsOf: url!, encoding: .ascii);
        let latestVersion = String(latestVersionRaw.filter { !" \n\t\r".contains($0) })
        return latestVersion == currentVersion
    } catch {
        print("Update Check Failure: \(error)");
    }
    
    return true;
}
